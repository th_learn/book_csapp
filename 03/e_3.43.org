# we doesn't care about width align
| expr               | type  | code                    |
|--------------------+-------+-------------------------|
| up->t1.u           | long  | movq (%rdi), %rax       |
|                    |       | movq %rax, (%rsi)       |
|--------------------+-------+-------------------------|
| up->t1.v           | short | movw 8(%rdi), %ax       |
|                    |       | movw %ax, (%rsi)        |
|--------------------+-------+-------------------------|
| &up->t1.w          | char* | leaq 10(%rdi), %rax     |
|                    |       | movq %rax, (%rsi)       |
|--------------------+-------+-------------------------|
| up->t2.a           | int*  | movq (%rdi), %rax       |
|                    |       | movq %rax, (%rsi)       |
|--------------------+-------+-------------------------|
| up->t2.a[up->t1.u] | int   | movq (%rdi), %rax       |
|                    |       | movq (%rdi), %rbx       |
|                    |       | movl (%rbx, %rax), %ecx |
|                    |       | movl %ecx, (%rsi)       |
|--------------------+-------+-------------------------|
| *up->t2.p          | char  | movq 8(%di), %rax       |
|                    |       | movl (%rax), %ebx       |
|                    |       | movl %ebx, (%rsi)       |
