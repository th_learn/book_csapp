* A
jump middle

* B
#+BEGIN_SRC c
  long fun_a(unsigned long x) {
      long val = 0;
      while (x > 0) {
          val = val ^ x;
          x = x >> 1;
      }
      return val & 0x1;
  }
#+END_SRC

* C
bit位有多少个1

奇数个 返回1
偶数个 返回0
