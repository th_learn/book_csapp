#+BEGIN_SRC c
  long test(long x, long y)
  {
      long val = 8 * x;
      if (y > 0) {
          if ((x - y) < 0) {
              val = x - y;
          } else {
              val = x + y;
          }
      } else if ((y + 2) <= 0) {
          // L2
          val = x + y;
      }
      return val;
  }
#+END_SRC
