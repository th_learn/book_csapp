/*
  The assember:
  (R[rdx]: R[rax]) / oprand = save quotient to rax, save remainder to rdx
 */
void remdic(long x, long y, long *qp, long *rp) {
    long q = x / y;
    long r = x % y;
    *qp = q;
    *rp = r;
}
