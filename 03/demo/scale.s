	.section	__TEXT,__text,regular,pure_instructions
	.build_version macos, 10, 14	sdk_version 10, 14
	.globl	_scale                  ## -- Begin function scale
	.p2align	4, 0x90
_scale:                                 ## @scale
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
    # rax = x + 4y
	leaq	(%rdi,%rsi,4), %rax
    # rcx = z + 2z = 3z
	leaq	(%rdx,%rdx,2), %rcx
    # rax = x + 4y + 3z * 4 = x + 4y + 12z
	leaq	(%rax,%rcx,4), %rax
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function

.subsections_via_symbols
