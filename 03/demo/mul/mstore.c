long mult2(long, long);
void mulstore(long x, long y, long *dst) {
    long t = mult2(x, y);
    *dst = t;
}
