# we assume that the exam says: "the disassember already handle the Two's complement, we just calculate hex to decimal"
|   |   hex | dec |
| A | 0x2e0 | 736 |
| B | -0x58 | -88 |
| C |  0x28 |  40 |
| D | -0x30 | -48 |
| E |  0x78 | 120 |
| F |  0x88 | 136 |
| G | 0x1f8 | 504 |
| H |  0xc0 | 192 |
| I |  0x48 | 72  |


