#+BEGIN_SRC c
  #include <limits.h>

  int divide_power2(int x, int k)
  {
      int is_neg = x & INT_MIN;
      (is_neg && (x = x + (1 << k) - 1));
      return x>> k;
  }
#+END_SRC
