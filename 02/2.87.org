bits: 1 5 10

| desc            | hex                        |           M | E                         | V                    | D               |
| -0              | hex(0b1 00000 00000000000) |           0 | 1-bias                    | 0                    | 0.0             |
| min > 2         | hex(0b0 10000 00000000001) |         1+e | 1                         | 1+e * 2              | 2.0?            |
| 512(2^9)        | hex(0b0 11000 00000000000) |           1 | 9                         | 512                  | 512.0           |
| max un-norm     | hex(0b0 00000 11111111111) |         1-e | 1-bias                    | (1-e) * (2^(1-bias)) | (2^(1-bias)).0? |
| -∞              | hex(0b1 11111 00000000000) |           - | -                         | -∞                   | -inf            |
| 0x3BB0(15280)() | 0x3BB0                     | 1.110111011 | numof(1101110110000)+bias | int(0x3BB0)          | int(0x3BB0).0   |

0x3BB0 = 0b11101110110000
