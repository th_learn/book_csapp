#+BEGIN_SRC c
  #include <limits.h>

  int signed_high_prod(int x, int y);

  unsigned unsigned_high_prod(unsigned x, unsigned y)
  {
      int xw_1 = !!(x & INT_MIN);
      int yw_1 = !!(y & INT_MIN);
      unsigned result  = signed_high_prod(x, y) + xw_1 * y + yw_1 * x + xw_1 + yw_1;
      return result;
  }
#+END_SRC
