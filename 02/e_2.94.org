#+BEGIN_SRC c
  typedef unsigned float_bits;
  #define BIAS 0x7f

  typedef struct _fp {
      unsigned sign;
      unsigned exp;
      unsigned frac;
  } fp;

  fp fp_from_float_bits(float_bits fb) {
      fp rst;
      rst.sign = fb >> 31;
      rst.exp = fb >> 23 & 0xff;
      rst.frac = fb & 0x7fffff;
      return rst;
  }

  float_bits fp_to_float_bits(fp *t_fp) {
      return t_fp->sign | t_fp->exp | t_fp->frac;
  }

  int fp_is_Nan(fp *t_fp) {
      return t_fp->exp == 0xff && t_fp->frac != 0;
  }

  float_bits float_twice(float_bits f) {
      fp t_fp = fp_from_float_bits(f);
      if (fp_is_Nan(&t_fp)) {
          return 0;
      }
      if (t_fp.exp <= BIAS) {
          t_fp.exp = (t_fp.exp << 2) - BIAS;
      }
      // ignore overflow
      return t_fp.sign | t_fp.exp | t_fp.frac;
  }
#+END_SRC
