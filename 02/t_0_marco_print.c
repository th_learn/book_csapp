#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

int main(int argc, char *argv[]) {
    int32_t x  = 10;
    uint64_t y = 88;
    printf("x = %" PRId32 ", y=%" PRIu64 "\n", x, y);
    exit(0);
}
