#include <stdlib.h>
#include <stdio.h>

void foo();
int x = 100;
int y = 100;

int main(int argc, char *argv[]) {
    foo();
    printf("x = %d; y = %xn", x, y);
    exit(0);
}
