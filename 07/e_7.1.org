| symbol | in .symtab? | type   | module | section   |
| buf    | y           | NOTYPE | m.o    | UND       |
| bufp0  | y           | OBJECT | swap.o | .data |
| bufp1  | y           | OBJECT | swap.o | COMMON    |
| swap   | y           | FUNC   | swap.o | .text     |
| temp   | n           | -      | -      | -         |
